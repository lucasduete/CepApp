# CepApp
Este é um APP em Android desenvolvido para a cadeira de PDM (Progração para Dispositivos Móveis) durante o curso de ADS (Análise e Desenvolvimento de Software) pelo IFPB campus Cajazeiras.
O objetivo deste projeto é criar um APP simples cuja a única função é fazer uma resquest para uma API de consulta de CEPs. Neste projeto é possível demonstrar diferentes formas de realizar essa request.
